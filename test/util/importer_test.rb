require 'minitest/autorun'
require './app/util/importer'

describe Importer do
  before :each do
    @row1 = Importer::MovieLocationsProvider::Row.new("Movie 1", 1234, nil, [])
    @row2 = Importer::MovieLocationsProvider::Row.new("Movie 2", 1234, "Embarcadero", ["Foo"])
    @row3 = Importer::MovieLocationsProvider::Row.new("Movie 1", 1234, "City Hall", ["Bar"])
    @row4 = Importer::MovieLocationsProvider::Row.new("Movie 1", 1234, "City Hall", ["Baz"])
    @rows = [@row1, @row2, @row3, @row4]

    # Prepare and mock the geocoder
    geocoder = Object.new
    def geocoder.geocode(address)
      return nil unless address
      return [0,0]
    end
    Location.geocoder = geocoder

    # prepare and mock the MovieLocationsProvider
    provider = MiniTest::Mock.new
    provider.expect(:load, @rows)
    @importer = Importer.new(provider)
  end

  it "should populate the movies database" do
    @importer.import

    movies = Movie.all
    movie1, movie2 = movies.sort_by(&:title)

    # assertions for movie1
    movie1.title.must_equal @row1.title
    movie1.actors.sort.must_equal ['Bar', 'Baz']
    movie1.locations.size.must_equal 1

    # assertions for movie2
    movie2.title.must_equal @row2.title
    movie2.actors.sort.must_equal ['Foo']

    movies.size.must_equal 2
  end
end
