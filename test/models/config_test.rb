require 'minitest/autorun'
require './app/util/config'

describe Config do
  it 'should always be running the test environment when running tests' do
    Config.env.must_equal 'test'
  end
end
