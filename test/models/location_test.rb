require './app/models/location'
require 'minitest/autorun'

describe Location do
  before :each do
    @geocoder = MiniTest::Mock.new
    Location.geocoder = @geocoder

    @location = Location.new(address: "foo street", lat: 1.5, lon: 4.5)
    @location.save!
  end

  after :each do
    Location.delete_all
  end

  describe 'geocode' do
    it 'will return a location if it already exists' do
      Location.geocode(@location.address).must_equal @location
    end

    it 'will return a geocoded location' do
      lat = 123
      lon = 234
      @geocoder.expect(:geocode, [lat, lon], [String])
      location = Location.geocode("some address")
      location.lat.must_equal lat
      location.lon.must_equal lon
    end

    it 'will return nil if the GeoCoder found no results' do
      @geocoder.expect(:geocode, nil, [String])
      Location.geocode("some address").must_equal nil
    end
  end
end
