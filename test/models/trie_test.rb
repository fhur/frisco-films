require 'minitest/autorun'
require './app/models/trie'
require 'benchmark'

describe Trie do
  before :each do
    @entry_chicken = Trie::Entry.new('chicken', '1')
    @entry_chickenator = Trie::Entry.new('chickenator', '2')
    @entry_chickpeas = Trie::Entry.new('chicpeas', '3')
    @entry_pizza = Trie::Entry.new('pizza', '4')
    @entry_pizzas = Trie::Entry.new('pizzas', '5')
    @entry_pie = Trie::Entry.new('pie', '6')
    @entry_pox = Trie::Entry.new('pox', '7')
    @entry_terminator = Trie::Entry.new('terminator', '8')
    @entry_terminal = Trie::Entry.new('terminal', '9')
    @entry_thermal = Trie::Entry.new('thermal', '10')

    @entries = [
      @entry_chicken,
      @entry_chickenator,
      @entry_chickpeas,
      @entry_pizza,
      @entry_pizzas,
      @entry_pie,
      @entry_pox,
      @entry_terminator,
      @entry_terminal,
      @entry_thermal
    ]

    @trie = Trie.new(@entries)
  end

  describe 'initialize' do
    it 'will create an empty trie when passed an empty list' do
      trie = Trie.new
      trie.height.must_equal 1
    end

    it 'must contain all entries passed as argument' do
      @entries.each do |entry|
        included = @trie.includes? entry.key
        included.must_equal true
      end
    end
  end

  describe 'find' do
    it 'returns nil if none was found' do
      @trie.find('lkajshdflkajshdf').must_equal []
    end

    it 'can return more than one entry if there are several matches' do
      results = @trie.find('chi')
      results.sort_by(&:key)
             .must_equal [@entry_chicken, @entry_chickenator, @entry_chickpeas]
    end
  end

  describe 'height' do
    it 'returns the height of the trie' do
      max_size = @entries.map { |e| e.key.size }.max
      @trie.height.must_equal(max_size + 1)
    end
  end

  describe 'benchmark' do
    # A very simple benchmar, given that initializing the Trie
    # is possibly the most expensive operation, make sure this
    # runs fast.
    NUM_ENTRIES = 10000
    it "can initialize #{NUM_ENTRIES} entries" do
      random_entry_provider = ->() do
        num_letters = (rand*100).to_i
        key = num_letters.times.map do
          min = 'a'.ord
          max = 'z'.ord
          (min + ((max-min) * rand)).to_i.chr
        end.join
        Trie::Entry.new(key, key)
      end

      puts ''
      puts "Creating #{NUM_ENTRIES} entries"
      entries = NUM_ENTRIES.times.map do
        random_entry_provider.call()
      end
      puts "Entries created, populating Trie..."

      benchmark = Benchmark.measure { Trie.new entries }
      puts "Finished creating Trie in #{'%.2f' % benchmark.real}s"
    end
  end
end
