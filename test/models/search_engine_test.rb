require 'minitest/autorun'
require './app/models/search_engine'

describe SearchEngine do
  before :each do
    @movie1 = Movie.new(title: "The Fight", year: 10, actors: ["Bob"])
    @movie2 = Movie.new(title: "The Big Fight", year: 10, actors: ["XX"])
    @movie3 = Movie.new(title: "The People", year: 10, actors: ["XY"])
    @movie4 = Movie.new(title: "Clashes", year: 10, actors: ["Y"])
    @movie5 = Movie.new(title: "Battle of Clans", year: 10, actors: ["Z"])
    @movie6 = Movie.new(title: "Fighting Clans", year: 10, actors: ["W"])
    @movies = [@movie1, @movie2, @movie3, @movie4, @movie5, @movie6]

    @se = SearchEngine.new
  end

  after :each do
    Movie.delete_all
  end

  describe 'initialize' do
    it 'initializes a new empty SearchEngine' do
      se = SearchEngine.new
      se.search("foo").must_equal []
      se.search("bar").must_equal []
    end
  end

  describe 'search' do
    before :each do
      @se.index(@movies)
    end

    it 'can search movies by actor' do
      @se.search("Bob").map(&:movie_id).must_equal [@movie1.id]
    end

    it 'can search movies by title' do
      @se.search("the").map(&:movie_id).must_equal [@movie1.id, @movie2.id, @movie3.id]
    end

    it 'search case insensitive' do
      @se.search("cLa").map(&:movie_id).must_equal [@movie4.id, @movie5.id, @movie6.id]
    end
  end
end
