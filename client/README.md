# Frisco Films Client

A simple Angular JS + Google Maps application that calls the Frisco Films API.

# Installation

Make sure you have bower and npm installed.

To install all dependencies:
```
$ npm i
```

You might also have to run `bower install`.

To start the development server:
```
$ npm start
```

## Whats Missing ?

Unfortunately I didn't have a lot of time left for the client which means that:
1. No tests.
2. Resources are not minified or optimized.
3. Every file is fetched on its own, which also makes loading a bit slow.

