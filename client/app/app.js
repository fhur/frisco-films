'use strict';

angular.module('friscoFilms', [
  'ngRoute',
  'friscoFilms.map'
])
.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $routeProvider.otherwise({redirectTo: '/map'});
}])
