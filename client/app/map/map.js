'use strict';

angular.module('friscoFilms.map', ['ngRoute', 'uiGmapgoogle-maps'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/map', {
    templateUrl: 'map/map.html',
    controller: 'MapCtrl'
  });
}])

/* Configuration for Angular Google Maps */
.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyBLZQEcCPV0R7NU_kBBTGw3LQmlC5UW4Us',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
})

/* The main Controller of the application */
.controller("MapCtrl", function($scope, movieService, geoService) {
    /* Map options and configuration */
    $scope.map = {
      zoom: 12,
      center: { latitude: 37.773972, longitude: -122.431297 },
      options: { disableDefaultUI: true, zoomControl: true },
      bounds: {
        southwest: { latitude: 37.773972, longitude: -122.431297 },
        northeast: { latitude: 37.773972, longitude: -122.431297 }
      }
    };
    /* Search suggestion results are stored here */
    $scope.suggestions = [];
    /* The current search query */
    $scope.search = "";
    /* An array of MovieLocations */
    $scope.movieLocations = [];

    $scope.loadSuggestions = function() {
      if ($scope.search.length > 0) {
        movieService.suggest($scope.search, function(suggestions){
          $scope.suggestions = suggestions;
        })
      }
    }

    /* searches for movies given a list of suggestiosn as input */
    $scope.searchMovies = function(suggestions) {
      var ids = suggestions.map(function(suggestion) {
        return suggestion.movie_id;
      });
      movieService.find(ids, function(movieLocations) {
        $scope.movieLocations = movieLocations;
        $scope.suggestions = [];
        $scope.search = "";
        reCalculateBounds(movieLocations);
      });
    }

    /* Selects or un-selects a movieLocations */
    $scope.selectLocation = function(clickedMovieLocation) {
      $scope.selectedMovieLocation = clickedMovieLocation === $scope.selectedMovieLocation ? null : clickedMovieLocation;
      $scope.$apply()
    }

    /*
     * Given a list of movie locations as input, returns an object
     * describing the new bounds for the map
     */
    var reCalculateBounds = function(movieLocations) {
      if (movieLocations.length > 0) {
        var newBounds = geoService.findBounds(_.map(movieLocations, 'coords'));
        console.log(newBounds);
        $scope.map.bounds = newBounds;
      }
    }
})

/**
 * A utility service with functions for operating with coordinates
 */
.factory("geoService", function(){
  /**
   * Given an array of coordinates, this method finds the smallest possible
   * bounding box that surrounds all coordinates or an empty bounding box
   * of size 0.
   *
   * - [Array of Coord] coords an array of objects with latitude and longitude as keys.
   */
  var findBounds = function(coords) {
    var north = _.maxBy(coords, 'latitude').latitude || 0;
    var south = _.minBy(coords, 'latitude').latitude || 0;
    var east = _.maxBy(coords, 'longitude').longitude || 0;
    var west = _.minBy(coords, 'longitude').longitude || 0;

    return {
      northeast: { latitude: north, longitude: east },
      southwest: { latitude: south, longitude: west }
    }
  }

  return {
    findBounds: findBounds
  }
})

/**
 * A Client to the FriscoFilms movies API
 */
.factory("movieService", function($http){
  var root = "/movies";

  /**
   * Finds search suggestions given a query string as argument
   * - [String] query : A query string e.g. 'Golden Ga'
   * - [function] callback : a callback function takes takes as argument
   *   a list of movie suggestions.
   */
  var suggest = function(query, callback) {
    var config = {
      params: { query: query }
    }
    $http.get(root + "/search", config)
      .then(function(response) {
        callback(response.data)
      }, function(response) { callback([]) } )
  }

  /**
   * Finds all movies with the given ids
   * - [Array of String] ids : a set of ids
   * - [function] : a callback function invoked upon successfull
   *   execution. The callback takes as argument an array of MovieLocations
   */
  var find = function(ids, callback) {
    var config = {
      params: { ids: ids.join(",") }
    };

    $http.get(root + "/find", config)
      .then(function(response) {
        var movieLocations = _.flatMap(response.data, movieToMovieLocations);
        callback(movieLocations);
      });
  }

  /* internal function that converts a Movie as returned by the API into an
   * array of movie locations */
  var movieToMovieLocations = function(movie) {
    return movie.locations.map(function(loc){
      var coords = { latitude: loc.lat, longitude : loc.lon };
      return {
        id: movie.id + '-' + loc.id,
        title: movie.title,
        year: movie.year,
        actors: movie.actors,
        coords: coords,
        address: loc.address
      }
    })
  }

  return {
    suggest : suggest,
    find : find
  }
})

/**
 * A simple directive that calls a function that adds a "on-enter='expr'" directive which is
 * called when the user presses the enter key.
 */
.directive('onEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keypress", function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
            scope.$eval(attrs.onEnter);
        });
        event.preventDefault();
      }
    });
  };
});
