# A object that describes the app's configuration
class Config
  class << self
    # Obtains the name of the current environment.
    def env
      ENV['RACK_ENV'] || 'development'
    end

    # Obtains the name of the database
    def database
      "movies-#{env}"
    end

    # Sets the RACK_ENV to 'test'
    def test!
      ENV['RACK_ENV'] = 'test'
    end

    # the google maps api key
    def maps_api_key
      ENV['MAPS_API_KEY']
    end
  end
end
