require 'httparty'
require 'csv'
require './app/models/movie'
require './app/models/location'
require './app/models/search_engine'
require './app/util/log_factory'

# A simple class that calls the data.sfgov.org API, parses the result and
# persists it in the local database.
class Importer
  # Provides movie locations by calling the data.sfgov.org movies API.
  class MovieLocationsProvider
    Row = Struct.new(:title, :release_year, :locations, :actors) do
      def valid?
        [title, release_year, locations].reduce(true) do |valid, field|
          field && field.size > 0
        end
      end
    end

    def load
      response = fetch
      response.map do |row|
        title = row['title'] || ''
        release_year = row['release_year'] || ''
        locations = row['locations'] || ''
        actor1 = row['actor_1']
        actor2 = row['actor_2']
        actor3 = row['actor_3']
        valid_actors = [actor1, actor2, actor3].select { |a| a }.map(&:strip)
        Row.new(title.strip, release_year.strip, locations.strip, valid_actors)
      end.select(&:valid?)
    end

    private

    def fetch
      response = HTTParty.get('https://data.sfgov.org/resource/wwmu-gmzc.json')
      raise "Error loading movies:\n #{response.body}" unless response.code == 200
      response
    end
  end

  LOG = LogFactory.create

  def initialize(locations_provider=MovieLocationsProvider.new)
    @locations_provider = locations_provider
  end

  # Loads and imports a set of movies
  def import
    response = @locations_provider.load
    parse(response)
  end

  private

  def parse(rows)
    movies = rows.group_by { |row| [row.title, row.release_year] }
    .map do |title_year, rows|
      title, year = title_year
      LOG.debug "Parsing movie #{title}, #{year}"
      actors = parse_actors(rows)
      locations = parse_locations(rows)

      # Find an existing movie to update, or create a new one
      movie = Movie.find_by_title(title) || Movie.new
      movie.title = title
      movie.year = year
      movie.actors = actors
      movie.location_ids = locations.map(&:id)
      movie
    end

    movies.each do |movie|
      movie.save
      unless movie.errors.empty?
        LOG.error "Failed to save #{movie.title} due to: #{movie.errors.messages}"
      end
    end
    movies
  end

  def parse_actors(rows)
    Set.new(rows.flat_map(&:actors))
  end

  def parse_locations(rows)
    locations = rows.map { |row| Location.geocode(row.locations) }
      .select { |r| r }
    Set.new locations
  end
end

