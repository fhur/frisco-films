require 'logger'

# A factory for creating Logger objects
class LogFactory
  # Return a new, fully configured Logger instance
  def self.create
    Logger.new(STDOUT)
  end
end
