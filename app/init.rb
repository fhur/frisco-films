require 'i18n'
require 'mongo_mapper'
require './app/models/search_engine'
require './app/util/log_factory'
require './app/util/config'

##############################################
######### Initialization Script ##############
##############################################

# This script is the starting/bootstraping point in the application.

LOG = LogFactory.create

LOG.info "Initializing on #{Config.env}..."

LOG.info 'Setting up I18n'
I18n.config.available_locales = [:en]

LOG.info 'Connecting to database'
MongoMapper.connection = Mongo::Connection.new('localhost', 27017)
MongoMapper.database = Config.database

LOG.info 'Indexing'
SearchEngine.instance.index
LOG.info 'Initializaition complete'
