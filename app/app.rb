require 'sinatra/base'
require 'json'
require './app/models/search_engine'
require './app/models/movie'
require './app/init'

# Exposes the movie locations API
class App < Sinatra::Application

  # Allow static content to be served by sinatra.
  set :static, true
  # Set the location for the static content. This is where
  # the single page application will live.
  set :public_folder, './public/'

  def initialize
    super
    @search_engine = SearchEngine.instance
  end

  before do
    # make the content type :json by default
    content_type :json
  end

  # Serve the Single Page Application when calling '/', normally this could
  # be handled by the web server (apache, nginx), but for now this will do.
  get '/' do
    content_type 'text/html'
    send_file File.join(settings.public_folder, 'index.html')
  end

  # Return a list of movies given a search query
  get '/movies/search' do
    query = params['query']
    if query && query.size > 0
      [200, @search_engine.search(query).to_json]
    else
      [400, {error: 'Invalid query parameter'}.to_json]
    end
  end

  # Return a list of locations given a movie id
  get '/movies/find' do
    ids_param = params['ids']
    if ids_param
      ids = ids_param.split(',')
      movies = Movie.find_by_ids(ids)
      [200, movies.to_json]
    else
      [400, {error: 'Invalid ids parameter'}]
    end
  end
end
