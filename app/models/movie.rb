require 'mongo_mapper'
require './app/models/location'

# Represents a movie. A movie can have many locations.
class Movie
  include MongoMapper::Document

  key :title,        String, required: true, unique: true
  key :year,         Integer, required: true
  key :actors,       Array, required: true
  key :location_ids, Array, required: true

  # Return a list of locations where this movie was shot.
  def locations
    Location.where(:_id => {:$in => location_ids})
  end

  # used to render a movie when calling to_json
  def as_json(options = {})
    {:id        => id,
     :title     => title,
     :year      => year,
     :actors    => actors,
     :locations => locations}
  end

  class << self
    # Returns all movies with the given IDs.
    # - ids: An arrya of movie IDs.
    def find_by_ids(ids)
      where(:_id => {:$in => ids}).all
    end
  end
end


