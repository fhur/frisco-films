require 'httparty'
require './app/util/log_factory'
require './app/util/config'

#
# A Simple GeoCoder based on the Google Maps Geocoding API
#
# Usage:
# geocoder = GeoCoder.initialize(apikey: API_KEY,
#                                bounds: [[1,2],[3,4]],
#                                region: 'us',
#                                city: 'Santa Monica')
# geocoder.geocode("Foo Street 123")
class GeoCoder
  include HTTParty
  base_uri 'https://maps.googleapis.com/maps/api/'

  LOG = LogFactory.create

  # Creates a new GeoCoder
  #
  # - api_key: the google maps API key.
  # - bounds: An array with to coordinate tuples with the northeast and southwest bounds.
  # - region: Helps localize searches.
  # - city: the name of the city, even more help for localizing searches.
  #         Will append the city name to every query.
  def initialize(api_key:, bounds:, region:, city:)
    @api_key = api_key
    @bounds = bounds
    @city = city
  end

  # Finds the coordinate pair corresponding the given address or nil if not found
  # - address: A string representing an address.
  def geocode(address)
    p1, p2 = @bounds
    params = {
      # Hack: appending the city greatly improves geocoding results
      address: "#{address}, #{@city}",
      key: @api_key,
      region: @region,
      bounds: "#{p1[0]},#{p1[1]}|#{p2[0]},#{p2[1]}"
    }

    response = nil
    begin
      response = self.class.get('/geocode/json', query: params)
    rescue HTTParty::Error => e
      LOG.error "Failed to geocode #{address}: #{e}"
      return nil
    end

    if response.success?
      results = response['results']
      return nil unless results.first
      point = results.first['geometry']['location']
      return [point['lat'], point['lng']]
    else
      LOG.debug "No result found for #{address}"
      return nil
    end
  end

  # Factory method for a new instance of GeoCoder based on San Francisco
  def self.san_fran
    # To improve geocoding results in San Francisco, these coordinates
    # define a bounding box around San Francisco.
    bounds = [
      [38.0499751224454, -121.40218260117189],
      [37.68788726563633, -123.15999510117189]
    ]
    GeoCoder.new(api_key: Config.maps_api_key,
                 bounds: bounds, region: 'us', city: 'San Francisco')
  end
end


