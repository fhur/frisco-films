require 'i18n'
require_relative 'trie'
require './app/models/movie'
require 'concurrent'

# A very simple attempt at implementing a thread safe Trie based search engine
#
# Usage is very simple: to index call the SearchEngine#index method
# which will load all Movies and create the underlying Trie.
# Searching is done by the SearchEngine#search method which will return a set of
# Movie objects given a search term.
class SearchEngine

  attr_reader :trie

  def initialize
    @entry_generators = [
      MovieTitleEntryGenerator.new,
      ActorNameEntryGenerator.new,
      ActorLastNameEntryGenerator.new,
      # You may supply additional entry generators like a
      # MovieAddressEntryGenerator which would allow searching
      # movies by address or street names.
    ]
    # hold an atomic reference to the trie, this way we can
    # swap the reference when indexing without affecting.
    # running search queries
    @trie = Concurrent::AtomicReference.new(Trie.new)

    # The maximum number of results returned by the #search method
    @num_results = 10
  end

  # Performs a search for the given query, returns a list
  # of EntryData
  def search(query)
    entries = @trie.get.find(query.strip.downcase)
    # After finding entries in the Trie, perform a simple 'result ranking'
    # algorithm by sorting by the entries that got the most results.
    entries.group_by { |entry| entry.data.movie_id }
           .sort_by { |movie_id, entries| entries.size }
           .first(@num_results)
           .map { |movie_id, entries| entries.first.data }
  end

  def index(movies = Movie)
    entries = []

    movies.each do |movie|
      entries += @entry_generators.flat_map do |entry_gen|
        entry_gen.generate_entries(movie)
      end
    end

    trie = Trie.new(entries)
    # atomically change the trie reference
    @trie.set(trie)
  end

  # Generates entries for a movie based on the movie title.
  class MovieTitleEntryGenerator
    def generate_entries(movie)
      words = movie.title.to_normalized_words.drop(1)
      words << movie.title.normalize
      words.map do |word|
        Trie::Entry.new(word,
          EntryData.new(movie.id, movie.title, :title, movie.title))
      end
    end
  end

  # Generates entries for a movie based on the actor last names.
  class ActorLastNameEntryGenerator
    def generate_entries(movie)
      movie.actors.flat_map do |actor|
        actor.to_normalized_words.map do |word|
          Trie::Entry.new(word,
            EntryData.new(movie.id, movie.title, :actor, actor))
        end
      end
    end
  end

  # Generates entries for a movie based on the movie's actors.
  class ActorNameEntryGenerator
    def generate_entries(movie)
      movie.actors.map do |actor|
          Trie::Entry.new(actor.normalize,
            EntryData.new(movie.id, movie.title, :actor, actor))
      end
    end
  end

  # A struct used to represent search results.
  # - movie_id: the id of the movie represented by this search result.
  # - title: the title of the movie represented by this search result.
  # - type: the type of object that was used to find the movie.
  # - phrase: the search query or phrase.
  #
  # Example:
  #
  # EntryData.new('1234', "The Best Movie", :actor, 'George Clooney')
  EntryData = Struct.new(:movie_id, :title, :type, :phrase)

  # Return a reference to the static SearchEngine instance
  def self.instance
    @inst ||= SearchEngine.new
  end
end

# Monkeypatch String
class String
  def normalize
    I18n.transliterate(self)
    .downcase
    .gsub(/\s+/, ' ')
  end

  def to_normalized_words
    normalize.split(' ')
  end
end
