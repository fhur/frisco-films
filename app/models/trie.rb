# An immutable Trie implementation.
#
# Every Node in the Trie has a list of Entry items. This
# class provides two main operations: initialize and find.
#
# initialize: initializes the Trie with a list of entries.
# find: returns all entries that match a given query.
class Trie
  # An Entry is a simple datastructure with the following keys:
  # - key or word:
  # - type: Indicating what type of object is being stored in
  # the entry (e.g. :movies, :cars, :foo, :bar)
  # - id: An identifier which can later be used to find the
  # stored element.
  Entry = Struct.new(:key, :data)

  # Internal datastructure for nodes inside the Trie
  Node = Struct.new(:substring, :items, :children)

  def initialize(entries = [], max_results: 100)
    @max_results = max_results
    @root = Node.new('', [], {})

    entries.each do |e|
      add_entry(e)
    end
  end

  # Returns an array of all Entry that match the given query.
  def find(query)
    chars = query.chars
    node = @root
    until chars.empty?
      letter = chars.shift
      child = node.children[letter]
      return [] unless child
      node = child
    end
    node.items.first(@max_results)
  end

  # return
  def includes?(query)
    !find(query).empty?
  end

  # Returns the height of the tree as an integer.
  def height
    return @height if @height
    max_height = 1
    next_nodes = [[@root, 1]]
    until next_nodes.empty?
      node, height = next_nodes.shift
      max_height = height if height > max_height
      node.children.values.each do |child|
        next_nodes << [child, height + 1]
      end
    end
    @height = max_height
  end

  private

  def add_entry(entry)
    node = @root
    i = 0
    chars = entry.key.chars
    while i < chars.size
      letter = chars[i]
      node = node.children[letter] ||= Node.new(entry.key[0..i], [], {})
      node.items << entry
      i += 1
    end
  end
end
