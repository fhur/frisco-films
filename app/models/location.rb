require 'mongo_mapper'
require_relative 'geocoder'

# Represents a location where a Movie was shot.
class Location
  include MongoMapper::Document

  # A string representing the address where the movie took place
  key :address,   String, required: true
  # The latitude where the movie took place
  key :lat,       Float, required: true
  # The longitude where the movie took place
  key :lon,       Float, required: true

  class << self
    # Allow clients to pass their own geocoder, also useful for tests.
    attr_accessor :geocoder

    # Given an String address as argument, it will
    # return the first location that matches the given address if found,
    # otherwise it will transform the address into a (lat, lon) pair
    # and persist a Location object.
    def geocode(address)
      # find a location with the given address
      location = Location.where(address: address).first
      return location if location

      # If not found, then geocode it.
      @geocoder ||= GeoCoder.san_fran
      result = @geocoder.geocode(address)

      # if there are no results, then return nil
      return nil unless result

      # if a result was found, then persist it
      lat, lon = result
      location = Location.new(address: address, lat: lat, lon: lon)
      location.save!
      location
    end
  end
end
